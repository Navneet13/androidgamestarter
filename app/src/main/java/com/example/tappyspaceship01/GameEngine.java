package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    //score
    int score = 0;
    int lives = 3;

    String fingerAction = "";
    // ----------------------------
    // ## SPRITES
    // ----------------------------
      Player player = new Player(this.getContext(),1700,20);


      ArrayList<Item> candies = new ArrayList<>();

      Item candy = new Item(this.getContext(), 50,20);
      Item rainbow = new Item(this.getContext(),50,200);
      Item poop = new Item(this.getContext(),50,400);

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();
    }


    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen

    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {

                 //Move the Item

                 int newCandyXPosition = candy.getxPosition() + 25;
                 candy.setxPosition(newCandyXPosition);

        int newPoopXPosition = poop.getxPosition() + 15;
        poop.setxPosition(newPoopXPosition);
        int newRainbowXPosition = poop.getxPosition() + 35;
        rainbow.setxPosition(newRainbowXPosition);

                 // Move the Item's hitbox
                 int candyHitBox = candy.getHitbox().left + 25;
                 int poopHItBox = poop.getHitbox().left + 25;
                 int rainbowHitBox = rainbow.getHitbox().left + 25;

                 //Moving the player up and down

                  int midScreen = screenHeight/2;
                  if (fingerAction == "mouseDown") {
                      if (player.getyPosition() <= midScreen) {
                          int newPlayerYPosition = player.getyPosition() - 10;
                          player.setyPosition(newPlayerYPosition);
                      } else if (player.getyPosition() > midScreen) {
                          int newPlayerPosition = player.getyPosition() + 10;
                          player.setyPosition(newPlayerPosition);
                      }
                  }


                  //Detecting the collision
                   if (player.getHitbox().intersect(candy.getHitbox()) == true || player.getHitbox().intersect(candy.getHitbox()) == true ) {
                        score = score + 1;
                    }

                   else
                       if(player.getHitbox().intersect(poop.getHitbox()) == true){
                           lives = lives - 1 ;
                       }


                 }


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.BLACK);



            int rectTop = 150;
            int rectBottom = 190;
            for(int i=0;i<4;i++)
            {
                canvas.drawRect(50, rectTop, 1500, rectBottom, paintbrush);
                rectTop = rectTop + 200;
                rectBottom = rectTop+ 40;
            }
//            canvas.drawRect(50, 100, 1500, 140, paintbrush);
//            canvas.drawRect(50, 300, 1500, 340, paintbrush);
//            canvas.drawRect(50, 500, 1500, 540, paintbrush);
//            canvas.drawRect(50, 700, 1500, 740, paintbrush);

            canvas.drawText("Lives: "+ lives, 1200, 50, paintbrush);
            canvas.drawText("Score: "+ score,1400,50,paintbrush);

            paintbrush.setTextSize(40);


            // draw player graphic on screen
            canvas.drawBitmap(player.getImage(),player.getxPosition(), player.getyPosition(), paintbrush);






//            Random r  = new Random();
//            int randomNumber = r.nextInt();
//            for(int i = 0; i < randomNumber; i++){
//                candies.add(candy);
//            }


            //Draw the enemy on the screen
//
//            int sizeOfCandyArray = candy.size();
//            for(int i = 0;i< candies.size();i++);
//            {
                canvas.drawBitmap(candy.getImage(),candy.getxPosition(), candy.getyPosition(), paintbrush);
                canvas.drawBitmap(rainbow.getImage(),rainbow.getxPosition(),rainbow.getyPosition(),paintbrush);
                canvas.drawBitmap(poop.getImage(),poop.getxPosition(),poop.getyPosition(),paintbrush);

//            }
//                canvas.drawBitmap(rainbow.getImage(),rainbow.getxPosition(),rainbow.getyPosition(),paintbrush);
////                canvas.drawBitmap(poop.getImage(),rainbow.getxPosition(),rainbow.getyPosition(),paintbrush);
//            }

//            Random r1  = new Random();
//            int randomNumber1 = r1.nextInt();
//            for(int i = 0;i< randomNumber1;i++){
////                canvas.drawBitmap(candy.getImage(),candy.getxPosition(), candy.getyPosition(), paintbrush);
//                canvas.drawBitmap(rainbow.getImage(),rainbow.getxPosition(),rainbow.getyPosition(),paintbrush);
////                canvas.drawBitmap(poop.getImage(),rainbow.getxPosition(),rainbow.getyPosition(),paintbrush);
//            }
//
//            Random r2  = new Random();
//            int randomNumber2 = r.nextInt();
//            for(int i = 0;i< randomNumber2;i++){
////                canvas.drawBitmap(candy.getImage(),candy.getxPosition(), candy.getyPosition(), paintbrush);
////                canvas.drawBitmap(rainbow.getImage(),rainbow.getxPosition(),rainbow.getyPosition(),paintbrush);
//                canvas.drawBitmap(poop.getImage(),rainbow.getxPosition(),rainbow.getyPosition(),paintbrush);
//            }



            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------




    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {

            fingerAction = "mousedown";
        }
        else if (userAction == MotionEvent.ACTION_UP) {

            fingerAction = "mouseup";
        }

        return true;
    }
}
